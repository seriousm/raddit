-- phpMyAdmin SQL Dump
-- Generation Time: Sep 07, 2017 at 01:18 PM

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `twooh`
--
CREATE DATABASE IF NOT EXISTS `twooh` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `twooh`;

-- --------------------------------------------------------

--
-- Table structure for table `feeds`
--

CREATE TABLE IF NOT EXISTS `feeds` (
  `id_feed` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user` varchar(24) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `bucket` varchar(16) NOT NULL,
  `cat` varchar(64) NOT NULL,
  `is_sticky` bit(1) NOT NULL DEFAULT b'0',
  `is_default` bit(1) NOT NULL DEFAULT b'0',
  `is_user` bit(1) NOT NULL DEFAULT b'0',
  `source` varchar(16) NOT NULL,
  `hasOpts` tinyint(1) NOT NULL DEFAULT '1',
  `url` varchar(255) NOT NULL,
  `label` varchar(128) NOT NULL,
  `descrip` varchar(512) NOT NULL,
  `dt_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `errors` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `is_imported` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`id_feed`),
  KEY `url` (`url`),
  KEY `user` (`user`),
  KEY `bucket` (`bucket`),
  KEY `is_sticky` (`is_sticky`),
  KEY `label` (`label`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=50535 ;

-- --------------------------------------------------------

--
-- Table structure for table `likes`
--

CREATE TABLE IF NOT EXISTS `likes` (
  `id_like` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user` varchar(32) NOT NULL,
  `bucket` varchar(16) NOT NULL,
  `cat` varchar(64) NOT NULL,
  `label` varchar(128) NOT NULL,
  `descrip` varchar(512) NOT NULL,
  `url` varchar(512) NOT NULL,
  `thumb` varchar(255) NOT NULL,
  `permalink` varchar(255) NOT NULL,
  `dt_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_like`),
  KEY `user` (`user`),
  KEY `bucket` (`bucket`),
  KEY `cat` (`cat`),
  KEY `label` (`label`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13495 ;

-- --------------------------------------------------------

--
-- Table structure for table `suggestions_rejected`
--

CREATE TABLE IF NOT EXISTS `suggestions_rejected` (
  `user` varchar(24) NOT NULL,
  `friend` varchar(24) NOT NULL,
  `bucket` varchar(16) NOT NULL,
  `feedURL` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `is_invalid` tinyint(1) NOT NULL DEFAULT '0',
  KEY `user` (`user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user_friends`
--

CREATE TABLE IF NOT EXISTS `user_friends` (
  `bucket` varchar(16) NOT NULL,
  `user` varchar(24) NOT NULL,
  `friend` varchar(24) NOT NULL,
  `rank` int(10) unsigned NOT NULL,
  KEY `friend` (`friend`),
  KEY `user` (`user`),
  KEY `bucket` (`bucket`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user_interests`
--

CREATE TABLE IF NOT EXISTS `user_interests` (
  `user` varchar(24) NOT NULL,
  `bucket` varchar(16) NOT NULL,
  `interest` varchar(32) NOT NULL,
  `rank` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `dt_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  KEY `user` (`user`),
  KEY `bucket` (`bucket`),
  KEY `interest` (`interest`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user_subs`
--

CREATE TABLE IF NOT EXISTS `user_subs` (
  `id_user_sub` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user` varchar(24) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `active` bit(1) NOT NULL DEFAULT b'0',
  `source` varchar(16) NOT NULL,
  `is_multi` bit(1) NOT NULL DEFAULT b'0',
  `name` varchar(24) NOT NULL,
  `url` varchar(255) NOT NULL,
  `label` varchar(255) NOT NULL,
  `descrip` varchar(512) NOT NULL,
  `dt_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_user_sub`),
  UNIQUE KEY `idx_user_name` (`user`,`name`),
  KEY `active` (`active`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=561088 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_suggestions`
--

CREATE TABLE IF NOT EXISTS `user_suggestions` (
  `id_suggestion` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user` varchar(24) NOT NULL,
  `friend` varchar(24) NOT NULL,
  `kind` varchar(16) NOT NULL,
  `bucket` varchar(16) NOT NULL,
  `source` varchar(16) NOT NULL,
  `thumb` varchar(255) NOT NULL,
  `label` varchar(255) NOT NULL,
  `descrip` varchar(512) NOT NULL,
  `url` varchar(255) NOT NULL,
  `permalink` varchar(255) NOT NULL,
  `hotScore` int(10) unsigned NOT NULL DEFAULT '0',
  `score` smallint(5) unsigned NOT NULL,
  `dt_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_suggestion`),
  KEY `url` (`url`),
  KEY `user` (`user`),
  KEY `bucket` (`bucket`),
  KEY `hotScore` (`hotScore`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3006002 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_upvotes`
--

CREATE TABLE IF NOT EXISTS `user_upvotes` (
  `id_user_upvote` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user` varchar(24) NOT NULL,
  `id_reddit` varchar(16) NOT NULL,
  `title` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `subreddit` varchar(24) NOT NULL,
  `permalink` varchar(255) NOT NULL,
  `author` varchar(24) NOT NULL,
  `thumb` varchar(255) NOT NULL,
  `dt_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_user_upvote`),
  UNIQUE KEY `idx_dupe_check` (`user`,`id_reddit`) COMMENT 'Don''t delete it again, asshole!',
  KEY `subreddit` (`subreddit`),
  KEY `dt_created` (`dt_created`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6345839 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_upvotes_dist`
--

CREATE TABLE IF NOT EXISTS `user_upvotes_dist` (
  `id_user_upvote` int(10) unsigned NOT NULL DEFAULT '0',
  `user` varchar(24) NOT NULL,
  `id_reddit` varchar(16) NOT NULL,
  `title` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `subreddit` varchar(24) NOT NULL,
  `permalink` varchar(255) NOT NULL,
  `author` varchar(24) NOT NULL,
  `thumb` varchar(255) NOT NULL,
  `dt_created` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;